##################################################
#
#
#
#
#   CREATED BY HENRY AND CONNOR :)
#
#
#
#
##################################################







import random

# ask the user to make flashcards
star_line = ''
card_content = ''
card = ''
joke = input('want to see a flashcard?: (yes/no)').lower()
if joke == 'yes':
    card_length = random.randint(50,75)
    # length
    for i in range(card_length):
        star_line += '**'
        if i == 0:
            card_content += '*'
        elif i == card_length-1:
            card_content += '*'
        else:    
            card_content += '  '
    # width
    for i in range(round(card_length/2)):
        if i == 0:
            print(star_line)
        else:
            print(card_content)
    print(star_line)
    

make_card = input('would you like to make flashcards? (yes/no):').lower()

flashcards = []
count = 0
score = 0

# make a new flashcard
while (make_card == 'yes'):
    count += 1
    side_01 = input('\nwhat is the question/term side of the flashcard?: ')
    side_02 = input('\nwhat is the answer side of the flashcard?: ')
    new_flashcard = [side_01, side_02]
    flashcards.append(new_flashcard)
    make_card = input('\nwould you like to make another flashcard? (yes/no): ').lower()

# test the skill
if flashcards:
    print('\n\n\tReady for the test?\n\n')

    # need to display a random flashcard
    while quit != 'yes':
        flashcard_delete = random.randint(0,count-1)
        # print first side of fc
        print(flashcards[flashcard_delete][0])
        # ask for user answer
        user_answer = input('\nwhat is the answer?: ')
        # if right
        if user_answer == flashcards[flashcard_delete][1]:
            print('correct!!!')
            score += 1
        # if wrong, you suck
        else:
            print('incorrect, the correct answer is:\n' + flashcards[flashcard_delete][1])
            score -= 100000
        del flashcards[flashcard_delete]
        if not flashcards:
            quit = 'yes'
        else:
            print('\nyour current score is: ' + str(score))
            quit = input('\nwould you like to quit? (yes/no): ')
        count -= 1

    print('your final score is: ' + str(score))

# show the user the flashcards they make

# how many cards they got right